package org.example;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Console;
import cn.hutool.core.lang.Opt;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.text.StrPool;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.hutool.log.StaticLog;
import cn.hutool.setting.Setting;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static cn.hutool.core.text.CharSequenceUtil.format;

/**
 * 文本纠错
 *
 * @author VampireAchao
 */
public class TextCorrection {

    /**
     * 地址与鉴权信息，配置文件中
     */
    public static final String HOST_URL;
    public static final String APP_ID;
    public static final String API_SECRET;
    public static final String API_KEY;

    static {
        // 加载配置文件
        Setting setting = new Setting("app.setting");
        HOST_URL = setting.get("HOST_URL");
        APP_ID = setting.get("APP_ID");
        API_SECRET = setting.get("API_SECRET");
        API_KEY = setting.get("API_KEY");
        setting.clear();
    }

    public static void main(String[] args) {
        Map<String, Object> inspect = inspect("Hutool似一个小而全的Java工具类库,通过静态万法封装，降低相咲API的学习城本，提蒿工作效率，使Java拥有函数式语言般的尤雅，让Java语言也何以“甜甜的”。");
        Console.log(inspect);
    }

    public static JSONObject inspect(String words) {
        String authUrl = getAuthUrl(HOST_URL, API_KEY, API_SECRET);
        String json = getRequestJson(words);
        String backResult = HttpUtil.post(authUrl, json);
        StaticLog.debug("文本纠错返回结果：" + backResult);
        JSONObject result = JSONUtil.parseObj(backResult);
        if (Opt.ofNullable(result.getJSONObject("header"))
                .map(header -> header.getInt("code"))
                .filter(code -> code.equals(0))
                .isEmpty()) {
            throw new IllegalStateException(CharSequenceUtil.format("文本纠错失败：{}", backResult));
        }
        String text = result.getJSONObject("payload").getJSONObject("result").getStr("text");
        String base64Decode = Base64.decodeStr(text);
        StaticLog.debug("text字段base64解码后纠错信息：" + base64Decode);
        return JSONUtil.parseObj(base64Decode);
    }

    public static String getAuthUrl(String hostUrl, String apiKey, String apiSecret) {
        URL url = URLUtil.url(hostUrl);
        String date = DateUtil.formatHttpDate(DateUtil.date());
        String signatureTemplate = "host: {}\ndate: {}\nPOST {} HTTP/1.1";
        String signatureBeforeSha = format(signatureTemplate, url.getHost(), date, url.getPath());
        String signature = SecureUtil.hmacSha256(apiSecret).digestBase64(signatureBeforeSha, false);
        String authorization = MapUtil.builder(new LinkedHashMap<>())
                .put("api_key", apiKey)
                .put("algorithm", "hmac-sha256")
                .put("headers", "host date request-line")
                .put("signature", signature)
                .build()
                .entrySet().stream().map(entry -> format("{}=\"{}\"", entry.getKey(), entry.getValue()))
                .collect(Collectors.joining(StrPool.COMMA + StrPool.C_SPACE));
        String queryString = HttpUtil.toParams(MapUtil.<String, Object>builder(new LinkedHashMap<>())
                .put("authorization", Base64.encode(authorization))
                .put("date", date)
                .put("host", url.getHost())
                .build(), StandardCharsets.UTF_8, true);
        return HttpUtil.urlWithForm(hostUrl, queryString, CharsetUtil.CHARSET_UTF_8, false);
    }


    public static String getRequestJson(String text) {
        return JSONUtil.toJsonStr(MapUtil.builder(new LinkedHashMap<>())
                .put("header", MapUtil.builder(new LinkedHashMap<>())
                        .put("app_id", APP_ID)
                        .put("status", 3)
                        .build())
                .put("parameter", MapUtil.builder(new LinkedHashMap<>())
                        .put("s9a87e3ec", MapUtil.builder(new LinkedHashMap<>())
                                .put("result", MapUtil.builder(new LinkedHashMap<>())
                                        .put("encoding", "utf8")
                                        .put("compress", "raw")
                                        .put("format", "json")
                                        .build())
                                .build())
                        .build())
                .put("payload", MapUtil.builder(new LinkedHashMap<>())
                        .put("input", MapUtil.builder(new LinkedHashMap<>())
                                .put("encoding", "utf8")
                                .put("compress", "raw")
                                .put("format", "plain")
                                .put("status", 3)
                                .put("text", Base64.encode(text))
                                .build())
                        .build())
                .build());
    }
}